package Testcase;

import static org.junit.Assert.*;

import java.util.Scanner;

import org.junit.Test;

import com.healthinsurance.premiumcalculation.bean.Customer;
import com.healthinsurance.premiumcalculation.bean.HabitsMetrics;
import com.healthinsurance.premiumcalculation.bean.HealthMetrics;
import com.healthinsurance.premiumcalculation.controller.PremiumCalculationAgeandGender;
import com.healthinsurance.premiumcalculation.controller.PremiumCalculationHabitMetrics;
import com.healthinsurance.premiumcalculation.controller.PremiumCalculationHealthMetrics;

public class PremiumCalculatorTest {
    
    int basePremium=5000;
    
	Customer customer = new Customer();
	HabitsMetrics habitsMetrics = new HabitsMetrics();
	HealthMetrics healthMetrics = new HealthMetrics();
	
	PremiumCalculationAgeandGender ageGenderPremium = new PremiumCalculationAgeandGender();
	PremiumCalculationHabitMetrics habitPremium = new PremiumCalculationHabitMetrics();
	PremiumCalculationHealthMetrics healthPremium = new PremiumCalculationHealthMetrics();

	
	@Test
	public void test() {
            
		customer.setName("Norman Gomes");
		customer.setGender("Male");
		customer.setAge(34);

		healthMetrics.setHypertension("No");	
		healthMetrics.setBloodPressure("No");	
		healthMetrics.setBloodSugar("No");	
		healthMetrics.setOverweight("No");	
		habitsMetrics.setSmoking("No");	
		habitsMetrics.setAlcohol("yes");	
		habitsMetrics.setDailyExercise("Yes");	
		habitsMetrics.setDrugs("No");
		
		Scanner scan=new Scanner(System.in);
		System.out.println("Please enter the expected premium for age");
		int age=scan.nextInt();
		
		System.out.println("Please enter the expected premium for gender");
		int gender=scan.nextInt();
		
		
		System.out.println("Please enter the expected premium for health");
		int health=scan.nextInt();
		
		System.out.println("Please enter the expected premium for habit");
		int habit=scan.nextInt();
		
		
		assertEquals(age, ageGenderPremium.calculatePremiumforAge(basePremium));
		
		assertEquals(gender, ageGenderPremium.calculatePremiumforGender(basePremium));
		
		assertEquals(health, habitPremium.calculatePremiumforHabits(basePremium));
		
		assertEquals(habit, healthPremium.calculatePremiumforHealth(basePremium));
	}

}
