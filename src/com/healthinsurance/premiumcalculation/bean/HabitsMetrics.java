package com.healthinsurance.premiumcalculation.bean;

public class HabitsMetrics {
	
	static String smoking;
	static String alcohol;
	static String dailyExercise;
	static String drugs;
	
	public String getSmoking() {
		return smoking;
	}
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	public String getAlcohol() {
		return alcohol;
	}
	public void setAlcohol(String alcohol) {
		this.alcohol = alcohol;
	}
	public String getDailyExercise() {
		return dailyExercise;
	}
	public void setDailyExercise(String dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	public String getDrugs() {
		return drugs;
	}
	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}
	
	

}
