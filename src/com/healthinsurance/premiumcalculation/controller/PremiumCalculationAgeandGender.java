package com.healthinsurance.premiumcalculation.controller;

import com.healthinsurance.premiumcalculation.bean.Customer;

public class PremiumCalculationAgeandGender {

	int agePremium = 0;
	int genderPremium = 0;

	Customer cus = new Customer();

	public int calculatePremiumforAge(int basePremium) {

		int age = cus.getAge();

		if (age < 18) {
			agePremium = basePremium;

		} else if (18 <= age && age < 25) {
			agePremium = basePremium * 10 / 100;

		} else if (25 <= age && age < 30) {
			agePremium = basePremium * 20 / 100;

		} else if (30 <= age && age < 35) {
			agePremium = basePremium * 30 / 100;

		} else if (35 <= age && age < 40) {
			agePremium = basePremium * 40 / 100;
			
		} else if (age >= 40) {
			agePremium = basePremium * 60 / 100;
		}

		return agePremium;

	}

	public int calculatePremiumforGender(int basePremium) {

		if (cus.getGender().equalsIgnoreCase("Male")) {

			genderPremium = (basePremium + agePremium) * 2 / 100;

		}

		return genderPremium;

	}

}
