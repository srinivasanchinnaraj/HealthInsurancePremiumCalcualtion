package com.healthinsurance.premiumcalculation.controller;

import java.util.Scanner;

import com.healthinsurance.premiumcalculation.bean.Customer;
import com.healthinsurance.premiumcalculation.bean.HabitsMetrics;
import com.healthinsurance.premiumcalculation.bean.HealthMetrics;
import com.healthinsurance.premiumcalculation.bean.InsuranceCoverage;

public class PremiumCalculator {

	static int basePremium = 5000;

	public static void main(String[] args) {

		Customer customer = new Customer();
		HabitsMetrics habitsMetrics = new HabitsMetrics();
		HealthMetrics healthMetrics = new HealthMetrics();
		InsuranceCoverage coverage=new InsuranceCoverage();

		PremiumCalculationAgeandGender ageGenderPremium = new PremiumCalculationAgeandGender();
		PremiumCalculationHabitMetrics habitPremium = new PremiumCalculationHabitMetrics();
		PremiumCalculationHealthMetrics healthPremium = new PremiumCalculationHealthMetrics();
		CoverageCalculation covCal=new CoverageCalculation();
		
		Scanner scan = new Scanner(System.in);

		System.out.println("Please Enter Name");
		customer.setName(scan.nextLine());

		System.out.println("Please Enter Gender");
		customer.setGender(scan.nextLine());

		System.out.println("Please Enter Age");
		customer.setAge(scan.nextInt());

		System.out.println("Current health");

		System.out.println("Please Enter Hyper tension ");
		healthMetrics.setHypertension(scan.next());

		System.out.println("Please Enter Blood Pressure ");
		healthMetrics.setBloodPressure(scan.next());

		System.out.println("Please Enter Blood Sugur ");
		healthMetrics.setBloodSugar(scan.next());

		System.out.println("Please Enter Overweight ");
		healthMetrics.setOverweight(scan.next());

		System.out.println("Habit");

		System.out.println("Please Enter Smoking Details ");
		habitsMetrics.setSmoking(scan.next());

		System.out.println("Please Enter Alchohol Details ");
		habitsMetrics.setAlcohol(scan.next());

		System.out.println("Please Enter Daily Excercise Details ");
		habitsMetrics.setDailyExercise(scan.next());

		System.out.println("Please Enter Drugs Details ");
		habitsMetrics.setDrugs(scan.next());
		
		System.out.println("Do you already have insurance coverage with us");
		coverage.setCoverge(scan.next());
		

		basePremium = basePremium + ageGenderPremium.calculatePremiumforAge(basePremium)
				+ ageGenderPremium.calculatePremiumforGender(basePremium);

		basePremium = basePremium + habitPremium.calculatePremiumforHabits(basePremium);

		basePremium = basePremium + healthPremium.calculatePremiumforHealth(basePremium);

		System.out.println("Health Insurance Premium for Mr/Ms." + customer.getName() + ": Rs." + basePremium);

		int discountcal=covCal.calculateCoverage(basePremium, coverage.getCoverge());
		
		System.out.println("Health Insurance Premium after coverage calculation for Mr/Ms." + customer.getName() + ": Rs." + discountcal);
		
	}

}
