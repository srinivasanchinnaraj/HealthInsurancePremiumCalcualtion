package com.healthinsurance.premiumcalculation.controller;

public class CoverageCalculation {
	int premiumdiscout = 50;
	int discountcal=0;

	public int calculateCoverage(int premium, String coverage) {

		if (coverage.equalsIgnoreCase("YES")) {

			discountcal = premium - (premium * premiumdiscout / 100);

		} else
			discountcal = premium + (premium * premiumdiscout / 100);

		return discountcal;

	}
}
