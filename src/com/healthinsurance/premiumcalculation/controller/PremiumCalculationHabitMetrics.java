package com.healthinsurance.premiumcalculation.controller;

import com.healthinsurance.premiumcalculation.bean.HabitsMetrics;


public class PremiumCalculationHabitMetrics {
	
	int habitsPremium=0;
	int goodHabit=3;
	int badHabit=3;
	
	HabitsMetrics habits=new HabitsMetrics();
	
	public int calculatePremiumforHabits(int basePremiunm ) {
		
		if(habits.getSmoking().equalsIgnoreCase("YES")) {
			habitsPremium=habitsPremium + (basePremiunm * badHabit/100);
		}else if(habits.getAlcohol().equalsIgnoreCase("YES")){
			habitsPremium=habitsPremium + (basePremiunm * badHabit/100);
		}else if(habits.getDailyExercise().equalsIgnoreCase("YES")){
			habitsPremium=habitsPremium -  (basePremiunm * goodHabit/100);
		}else if(habits.getDrugs().equalsIgnoreCase("YES")){
			habitsPremium=habitsPremium +  (basePremiunm * badHabit/100);
		}
		
		return habitsPremium;
		
	}
	
	

}
